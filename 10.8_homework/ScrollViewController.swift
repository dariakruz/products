//
//  ScrollViewController.swift
//  10.8_homework
//
//

import UIKit

class ScrollViewController: UIViewController {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var createEventButton: UIButton!
    @IBOutlet weak var orderButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()

    }
    
    func updateUI() {
        menuButton.layer.borderWidth = 3
        menuButton.layer.borderColor = UIColor.systemGreen.cgColor
        menuButton.layer.cornerRadius = 10
        
        orderButton.layer.borderWidth = 3
        orderButton.layer.borderColor = UIColor.systemGreen.cgColor
        orderButton.layer.cornerRadius = 10
        
        createEventButton.layer.cornerRadius = 10
        
        
    }
    



}
