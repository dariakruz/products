//
//  SettingsTableViewCell.swift
//  10.8_homework
//
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
    // добавляем идентификатор
    static let identifier = "SettingsTableViewCell"

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconContainer: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var detailsLabel: UILabel!
    
    private let mySwitch: UISwitch = {
        let mySwitch = UISwitch()
        mySwitch.onTintColor = .systemGreen
        return mySwitch
    }()
    
    private let notification: UIImageView = {
        let notification = UIImageView()
        notification.backgroundColor = .white
        notification.tintColor = .systemRed
        notification.image = UIImage(systemName: "1.circle.fill")
        return notification
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // 1. нужно определить размер контейнерa иконки
        let size: CGFloat = contentView.frame.size.height - 12
        iconContainer.frame = CGRect(x: 10, y: 6, width: size, height: size)
        iconContainer.layer.cornerRadius = 8
        iconContainer.clipsToBounds = true
        iconContainer.layer.masksToBounds = true
        
        nameLabel.numberOfLines = 1
        detailsLabel.numberOfLines = 1

        iconImageView.tintColor = .white
        iconImageView.contentMode = .scaleAspectFit
        
        mySwitch.sizeToFit()
        mySwitch.frame = CGRect(x: contentView.frame.size.width - mySwitch.frame.size.width - 20,
                               y: (contentView.frame.size.height - mySwitch.frame.size.height)/2,
                               width: mySwitch.frame.size.width,
                               height: mySwitch.frame.size.height)
        
        notification.frame = CGRect(x: contentView.frame.size.width - 30, y: (contentView.frame.size.height - 30)/2, width: 30, height: 30)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        // нужно ресетнуть все свойства, чтобы они не остались от предыдущей ячейки
        iconImageView.image = nil
        nameLabel.text = nil
        iconContainer.backgroundColor = nil
        detailsLabel.text = nil
        mySwitch.removeFromSuperview()
        notification.removeFromSuperview()
        accessoryType = .disclosureIndicator
    }
    
    public func configure(with model: SettingsOption) {
        nameLabel.text = model.title
        iconImageView.image = model.icon
        iconContainer.backgroundColor = model.iconBackgroundColor
        detailsLabel.text = model.details
        accessoryType = .disclosureIndicator
        if model.hasSwitcher {
            displaySwitcher()
        }
        if model.notification {
            displayNotification()

        }
    }
    
    func displaySwitcher() {
        accessoryType = .none
        contentView.addSubview(mySwitch)
        mySwitch.sizeToFit()
        mySwitch.frame = CGRect(x: contentView.frame.size.width - mySwitch.frame.size.width - 20,
                               y: (contentView.frame.size.height - mySwitch.frame.size.height)/2,
                               width: mySwitch.frame.size.width,
                               height: mySwitch.frame.size.height)
        
        mySwitch.isOn = true
    }
    
    func displayNotification() {
        contentView.addSubview(notification)
        notification.frame = CGRect(x: contentView.frame.size.width - 30, y: (contentView.frame.size.height - 30)/2, width: 30, height: 30)
    }
}
