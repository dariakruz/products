//
//  ViewController.swift
//  10.8_homework
//
//  Created by Lisa Kryshkovskaya on 3/20/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // сам скролвью своего размера не знает, поэтому обязательно нужно задать размер контента. Если задать ширину контента шире, чем родительский вью, то скролл будет и горризонтальным тоже.
        
        //contentView.layoutIfNeeded() //set a frame based on constraints
        scrollView.contentSize = CGSize(width: contentView.frame.size.width, height: contentView.frame.size.height)
        
//        scrollView.contentSize = CGSize(width: contentView.frame.width, height: contentView.frame.height)


//        scrollView.contentSize = CGSize(width: view.frame.size.width, height: CGFloat(2080))
    }
    



}

