//
//  GallaryViewController.swift
//  10.8_homework
//
//

import UIKit

class GallaryViewController: UIViewController {
    
    private let sectionInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
    private let itemsPerRow: CGFloat = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    let priceArray = ["3900 руб.", "5800 руб.", "4500 руб.", "18600 руб.", "7000 руб.", "8000 руб."]
    let discountPriceArray = ["3700 руб.", "4500 руб.", "3500 руб.", "15600 руб.", "6000 руб.", "7000 руб."]
    let productNameArray = ["ФУТБОЛКА 'KARMA'", "РУБАШКА SCANDI", "ФУТБОЛКА 'ALTERN 8'", "ЗИМНЯЯ КУРТКА WALKER", "СВИТШОТ DAKOTA", "ХУДИ BROOKLYN 'МНОГО ЛИЦ'"]
    let productImageArray: [UIImage?] = [UIImage(named: "1"), UIImage(named: "2"), UIImage(named: "3"), UIImage(named: "4"), UIImage(named: "5"), UIImage(named: "6")]
    var productNumber = 6
    
}

// два обязательных для реализации протокола, чтобы работать с коллекциями
extension GallaryViewController: UICollectionViewDataSource {
    
    func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      insetForSectionAt section: Int
    ) -> UIEdgeInsets {
      return sectionInsets
    }
    func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
      return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productNumber
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // сначала нужно получить ячейку и привести ее к типу, который мы создали для ячейки
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductName", for: indexPath) as! ProductCollectionViewCell
        
        // тут наполняем ячейку данными
        cell.imageView.image = productImageArray[indexPath.row]
        cell.nameLabel.text = productNameArray[indexPath.row]
        cell.priceLabel.text = priceArray[indexPath.row]
        cell.discountPriceLabel.text = discountPriceArray[indexPath.row]
        
        // добавляем зачеркнутый стиль для priceLabel
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cell.priceLabel.text!)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        cell.priceLabel.attributedText = attributeString
        
        // и возвращаем эту ячейку
        return cell
        
        // тут еще через сториборд нужно сказать коллекции, что данные она получает от вьюконтроллера. перетягиваем связь и выбираем dataSource
    }
}


// MARK: - Collection View Flow Layout Delegate
extension GallaryViewController: UICollectionViewDelegateFlowLayout {
  // 1 - метод, чтобы определить количество ячеек в ряду
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    
    // 2
    let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
    let availableWidth = view.frame.width - paddingSpace
    let widthPerItem = availableWidth / itemsPerRow

    return CGSize(width: widthPerItem, height: widthPerItem*1.6)
    // и после этого чтобы коллекция знала откуда брать разер нужно назначить delegate в сториборде. перетянуть от коллекции к контроллеру и выбрать delegate
  }
}

//    let width = (UIScreen.main.bounds.size.width / 2)

// В size inspector для коллекции и ячейки нужно было установить estimate size в none. И размер ячейки тип кастом)

