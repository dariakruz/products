//
//  TableViewController.swift
//  10.8_homework
//
//

import UIKit

struct Section {
    let title: String
    let options: [SettingsOption]
}

struct SettingsOption {
    let title: String
    let icon: UIImage?
    let iconBackgroundColor: UIColor
    let details: String
    let hasSwitcher: Bool
    let notification: Bool
    // хэндлер в виде замыкания, функция, что выполнить при нажатии на строку
    let handler: (() -> Void)
}

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableView: UITableView!
    
    // создам массив модели/данных (настроек)
    var models = [Section]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        // функция для ассоциации модели и массива настроек
        models.append(Section(title: "", options: [
            SettingsOption(title: "Авиарежим", icon: UIImage(systemName: "house"), iconBackgroundColor: UIColor.systemOrange, details: "", hasSwitcher: true, notification: false, handler: {
                
            }),
            SettingsOption(title: "Wi-Fi", icon: UIImage(systemName: "wifi"), iconBackgroundColor: UIColor.systemBlue, details: "Anvics-YOTA", hasSwitcher: false, notification: false, handler: {
                
            }),
            SettingsOption(title: "Bluetooth", icon: UIImage(systemName: "arrow.branch"), iconBackgroundColor: UIColor.systemBlue, details: "Вкл.", hasSwitcher: false, notification: false, handler: {
                
            }),
            SettingsOption(title: "Сотовая связь", icon: UIImage(systemName: "antenna.radiowaves.left.and.right"), iconBackgroundColor: UIColor.systemGreen, details: "", hasSwitcher: false, notification: false, handler: {
                
            }),
            SettingsOption(title: "Режим модема", icon: UIImage(systemName: "personalhotspot"), iconBackgroundColor: UIColor.systemGreen, details: "", hasSwitcher: false, notification: false, handler: {
                
            })
        ]))
        
        models.append(Section(title: "", options: [
            SettingsOption(title: "Уведомления", icon: UIImage(systemName: "arrow.rectanglepath"), iconBackgroundColor: UIColor.systemRed, details: "", hasSwitcher: false, notification: true, handler: {
                
            }),
            SettingsOption(title: "Звуки, тактильные сигналы", icon: UIImage(systemName: "speaker.wave.3.fill"), iconBackgroundColor: UIColor.systemPink, details: "", hasSwitcher: false, notification: false, handler: {
                
            }),
            SettingsOption(title: "Не беспокоить", icon: UIImage(systemName: "moon.fill"), iconBackgroundColor: UIColor.systemPurple, details: "", hasSwitcher: false, notification: false, handler: {
                
            }),
            SettingsOption(title: "Экранное время", icon: UIImage(systemName: "hourglass.bottomhalf.fill"), iconBackgroundColor: UIColor.systemPurple, details: "", hasSwitcher: false, notification: false, handler: {
                
            })
        ]))
        models.append(Section(title: "", options: [
            SettingsOption(title: "Основные", icon: UIImage(systemName: "gearshape"), iconBackgroundColor: UIColor.systemGray, details: "", hasSwitcher: false, notification: false, handler: {
                
            }),
            SettingsOption(title: "Пункт управления", icon: UIImage(systemName: "switch.2"), iconBackgroundColor: UIColor.systemGray, details: "", hasSwitcher: false, notification: false, handler: {
                
            }),
            SettingsOption(title: "Экран и яркость", icon: UIImage(systemName: "a"), iconBackgroundColor: UIColor.blue, details: "", hasSwitcher: false, notification: false, handler: {
                
            })
        ]))
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let section = models[section]
        return section.title
    }
    
    // реализуем обязательную функцию протокола UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models[section].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //
        let model = models[indexPath.section].options[indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.identifier, for: indexPath) as? SettingsTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(with: model)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        let model = models[indexPath.section].options[indexPath.row]
//        model.handler()
    }
}
